package ru.tsc.chertkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.model.IAbstractUserOwnerModelRepository;
import ru.tsc.chertkova.tm.model.AbstractFieldsModel;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractUserOwnerModelRepository<M extends AbstractFieldsModel>
        extends AbstractRepository<M> implements IAbstractUserOwnerModelRepository<M> {

    public AbstractUserOwnerModelRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull M model) {
        super.add(model);
    }

    @Override
    public abstract void clear();

    @Override
    @NotNull
    public abstract List<M> findAll();

    @Override
    @Nullable
    public abstract M findById(@NotNull String id);

    @Override
    public abstract int getSize();

    @Override
    public abstract void removeById(@NotNull String id);

    @Override
    public abstract void update(@NotNull M model);

    @Override
    public abstract void add(@NotNull String userId, @NotNull M model);

    @Override
    public abstract void clear(@NotNull String userId);

    @Override
    @NotNull
    public abstract List<M> findAll(@NotNull String userId);

    @Override
    @Nullable
    public abstract M findById(@NotNull String userId, @NotNull String id);

    @Override
    public abstract int getSize(@NotNull String userId);

    @Override
    public abstract void removeById(@NotNull String userId, @NotNull String id);

    @Override
    public abstract void update(@NotNull String userId, @NotNull M model);

}
