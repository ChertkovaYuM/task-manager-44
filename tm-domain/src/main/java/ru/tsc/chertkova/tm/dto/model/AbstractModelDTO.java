package ru.tsc.chertkova.tm.dto.model;

import lombok.*;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractModelDTO implements Serializable {

    @Id
    @NotNull
    @Column(name = "ID")
    private String id = UUID.randomUUID().toString();

}
